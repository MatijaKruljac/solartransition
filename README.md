# SolarTransition

[![CI Status](http://img.shields.io/travis/Matija Kruljac/SolarTransition.svg?style=flat)](https://travis-ci.org/Matija Kruljac/SolarTransition)
[![Version](https://img.shields.io/cocoapods/v/SolarTransition.svg?style=flat)](http://cocoapods.org/pods/SolarTransition)
[![License](https://img.shields.io/cocoapods/l/SolarTransition.svg?style=flat)](http://cocoapods.org/pods/SolarTransition)
[![Platform](https://img.shields.io/cocoapods/p/SolarTransition.svg?style=flat)](http://cocoapods.org/pods/SolarTransition)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

SolarTransition is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "SolarTransition"
```

## Author

Matija Kruljac, kruljac.matija@gmail.com

## License

SolarTransition is available under the MIT license. See the LICENSE file for more info.
